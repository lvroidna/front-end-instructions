# Front End Instructions

## General Instructions
- Use the [Front End Boilerplate Setup](https://github.com/roidna/boilerplate-frontend) repository to set up any front end project at the beginning.
- Don’t use Adobe Assets measurements (use Tape chrome extension to measure the PSD if using Adobe Assets on web or use Photoshop). When measuring spacing with Tape don’t use the snap-to option (use “S” key to disable it).
- You can also use the [PerfectPixel plugin for Chrome](https://chrome.google.com/webstore/detail/perfectpixel-by-welldonec/dkaagdgjmgdmbnecmcefdhjekcoceebi?hl=en) for more accurate and live comparisons between the PSD and the site you're working on.
- Measurement workflow: check the spacing in the PSD, check the current spacing on the page (after you set the font properties), calculate the difference, set the new CSS value, measure on the page if the new value is the same as the one in the PSD. When you finish a module compare the PSD and the page (opened in the browser with the same width as the PSD) side-by-side.
- Throughout the project, use relative paths that don't depend on the server's root folder because we don't know if the whole site will be hosted there or a sub-folder (e.g. `path/to/file` and not `/path/to/file`).
- Please use 2 spaces (not tabs) for code indentation.

## HTML Instructions
- Write [semantic HTML5](http://html5doctor.com/element-index/) markup ([article](http://diveintohtml5.info/semantics.html))
- Use the smallest `col-*` class (e.g `col-xs-12`) when defining the column count for all viewport sizes so that we can build on it easier in the future (e.g. just adding `col-sm-6` without having to add two classes).
- Add below meta tags between `<head>` tags for better SEO, they will be filled in later:
```html
<title></title>
<meta name="description" content="" />
<meta property="fb:page_id" content="" />
<meta property="fb:admins" content="" />
<meta property="og:site_name" content="VIPRE Antivirus and Internet Security" />
<meta property="og:url" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:image" content="" />
<meta property="twitter:site" content="@vipresecurity" />
<meta property="twitter:card" content="" />
<meta property="twitter:title" content="" />
<meta property="twitter:description" content="" />
<meta property="twitter:image:src" content="" />
<link rel="canonical" href="" />
```
- When implementing a form, don't forget to put all the form elements (textbox, select dropdown, submit button, etc) inside `<form>` tag and make sure the button is indicated with `<button type="submit">`, not `<a href="javascript:;">`

## CSS/LESS Instructions
- Use [SUIT CSS naming conventions](https://github.com/suitcss/suit/blob/master/doc/naming-conventions.md). Example:

```html
<section class="vp-Message">
  <header class="vp-Message-header">
    <h1 class="vp-Message-heading">Final message title here</h1>
    <h2 class="vp-Message-subheading">Additional message</h1>
  </header>
  <div class="vp-Message-body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  </div>
  <footer class="vp-Message-footer">
    <a class="vp-Button vp-Button--business" href="javascript:;">Try it free for business</a>
    <a class="vp-Button vp-Button--home" href="javascript:;">Try it free for home</a>
  </footer>
</section>
```
- Use [mobile-first](http://www.zell-weekeat.com/how-to-write-mobile-first-css/) approach with project-wide breakpoints
- Always set the line-height (if the designer set a weird one because the text in the designs is in one line set it to 1.2 without any units). Always check if the letter spacing is set ([Convert Photoshop letter spacing to CSS em/px](http://justinmarsan.com/css-letter-spacing-in-photoshop-and-browsers/)). Measure the paddings only after you already set all the font properties.
- Use padding instead of margins when possible (margins can collapse).
- For vertical spacing use bottom padding on all elements to position them in the module/section
.
- When styling an element use CSS classes instead of tags so that there is no extra work if we ever need to change a `<span>` tag to a `<a>` tag or an `<h2>` to a `<p>`. There are some exceptions for the elements that are often repeated and is unknown how many of them will exist on the live site with the CMS (e.g. `.dropdown-menu > li > a` in the Bootstrap’s dropdown menu or `<p>`’s inside the `<div>` container).
- Leave ids for the javascript selectors so there is no problem with [CSS specificity](http://www.smashingmagazine.com/2007/07/css-specificity-things-you-should-know/) when styling elements.
- Rather make a unique submodule class than writing long CSS selectors (or deeply nested in Less) with huge specificity. Those will be hard to override.
- Try to avoid using !important and use it only on [.is-state classes](https://github.com/suitcss/suit/blob/master/doc/naming-conventions.md#is-stateOfComponent) (like .is-active) when the state needs to override the style of a more complex CSS selector. You won’t normally have two states that tend to affect the same set of styles applied to the same module, so specificity conflicts from using `!important` should be few and far between.
- Rather than overriding Bootstrap’s CSS rules with !important just change the default values with [Less variables](http://getbootstrap.com/customize/#less-variables).
- Write time in CSS in seconds and without the leading zero (e.g. transition: color .4s).
- Don’t write browser specific prefixes like `-moz-border-radius` or `-webkit-border-radius` when the project has Autoprefixer in the gulp/grunt file.
- When using Flex property use the `.flex-row()`, `.flex-column()` and `.flex-grow()` mixins instead of relying on the Autoprefixer.
- Properties in the shorthand border property should be written in `1px solid #000` order. The same goes for the box-shadow (color property last).
- If an element needs an additional parent and you can’t find a good name for the CSS class use `.elementWrap` and if it needs a child element instead of the parent you can use `.element-content`.
- Don’t write :hover states for the mobile design so that the elements don't change color when swipe-scrolling down the page on the phone. Even Bootstrap modules should not change when they are hovered over on mobile (you can change the default hover states with [Less variables](http://getbootstrap.com/customize/#less-variables)).
- If there are no separate styles defined for :focus and :active states list them with the :hover rule to get the same styles (e.g. to override the default Bootstrap module :focus state).
- Use either descendant of modifier class, not both. It's harder to read. Modifier should be on the component and not the descendant (e.g. `.vp-Resources--dataSheet .vp-Resources-header {}`). When this is not possible with the current component define a new sub-component (e.g. `.vp-ResourcesBox.vp-ResourcesBox--dataSheet .vp-ResourcesBox-header {}`)
- For color variables in LESS do not use naming `@black`, `@black-2`, `@black-3` but rather specify with prepending `@color-*` and use [tools like this one](http://chir.ag/projects/name-that-color/) to get color name. Use numbering at the end only if you have to, for example:
```css
@color-mine-shaft: #333; 
@color-mine-shaft-2: #2b2b2b; // same color name but different HEX, numbering allowed
```
(to know which color it is, use Extension for IDE, e.g. [Color Highlighter for Sublime Text](https://github.com/Monnoroch/ColorHighlighter))
- When using Bootstrap’s Collapse component (or changing the height() of the element dynamically in general) adding the margin/padding to the element that is collapsing will cause a glitchy transition. Add the spacing to the descendant elements. If there is no other way you can replace the bottom spacing with the `:after { height: 29px; display: block; content: ''; visibility: hidden; }`
- ![jQuery Dimensions](/assets/bin/jquerydim.gif)

- Use Bootstrap’s [helper classes](http://getbootstrap.com/css/#helper-classes) and [utility mixins](http://getbootstrap.com/css/#less-mixins-utility) inside the Less file. Don’t add them to the HTML markup. Only [responsive utilities](http://getbootstrap.com/css/#responsive-utilities) are allowed in the HTML markup.
- Use display:flex on ul element instead of display:inline-block on li elements for inline lists to avoid [display:inline-block bug](http://codepen.io/chriscoyier/pen/hmlqF) ([IE10+](http://caniuse.com/#search=flexbox))
